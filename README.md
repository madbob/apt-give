# apt-give

This is the main repository of the apt-give project, published at https://apt.gives/

Data about packages are in the `data` folder, one file per package (named with the package's name itself).

Each file contains the following attributes:

- name: the name of the package, as appears in the `apt` repository
- homepage: the reference website of the packaged software, as indicated in the `apt` package. This is **not** intended to be modified in any other way than reading a new value from `apt` repository: if you want to suggest a different URL, please refer to the Debian maintainer of the package itself
- donate: the eventual link for donations, or `false` if none has been identified (yet...)
- added: the date the package has been added in this index
- updated: the date of the latest change of the file in this index
- to_be_reviewed: `false` when the package has already been reviewed. If `to_be_reviewed = false` and `donate = false`, it is assumed the package has no methods to receive donations
- broken_homepage: appears only when valorized to `true`, it is mostly a reminder for packages that need to be fixed on Debian

The reference list of packages is extracted from the `main` repository of amd64 architecture for Debian Stable, available [here](http://ftp.debian.org/debian/dists/bookworm/main/binary-amd64/Packages.gz), and is periodically updated.

## Method

If more than one package has the same `homepage`, the same `donate` value is applied to all of them. The same has been applied in many cases when the hosting website is the same (with obvious exceptions, like github.com).

Most recurring `homepage` values have been manually processed to find the donations page of the parent project (e.g. the many GCC packages refer to FSF), as well projects hosted by a well-known parent project (e.g. MetaCPAN).

The webpage in `donate` must be reachable from the `homepage` page (even if not directly, e.g. the project page links to a personal blog, which contains a donations page), to confirm the actual relation among the donations page and the project itself. No `donate` links are accepted if there is not a browse path from `homepage`. If the `donate` page is difficult to reach, or reachable after some jump, please explain the actual path while proposing it. Some exception may be applied when the relation between the package `homepage` and the donations pages is evident, despite lack of direct linking (e.g. UBPorts).

Online payment methods in "classic" currencies are preferred. If a donation procedure involves sending email to someone to obtain further informations, or only fixed tiers for business-oriented sponsorship programs are offered, or the only provided method is using a cryptocurrency, it is anyway difficult and complex to actually contribute.

Packages having an `homepage` on a GitHub repository have been automatically processed to find a "Sponsor" page of the owner (user or organization). This may not be the best option (e.g. KDE has a Sponsor page on GitHub, but also a more extensive webpage describing different donation methods), but at least the package has a donations link assigned; if better links are available, propose them.

If you are the maintainer of a package shipped on any Linux distribution (not just Debian), please consider to implement an [AppStream](https://www.freedesktop.org/software/appstream/docs/) metadata file and add there informations about [donations options](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url). Someday, I hope `apt-give` would become obsolete and superseeded by the same functionality directly embedded into the most popular package managers.
