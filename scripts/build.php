<?php

class Builder
{
    private $master;
    private $today;
    private $managed_urls;

    public function __construct()
    {
        $this->master = 'http://ftp.debian.org/debian/dists/bookworm/main/binary-amd64/Packages.gz';
        $this->today = date('Y-m-d');
        $this->managed_urls = [];
    }

    public function run()
    {
        $archive = $this->getData();
        $current = null;

        foreach($archive as $row) {
            $row = trim($row);

            if (str_starts_with($row, 'Package:')) {
                if ($current) {
                    $this->dumpPackage($current);
                }

                $current = [
                    'name' => preg_replace('/^Package: /', '', $row),
                    'homepage' => '',
                ];
            }
            else if (str_starts_with($row, 'Homepage:')) {
                if ($current) {
                    $current['homepage'] = preg_replace('/^Homepage: /', '', $row);
                }
            }
        }
    }

    private function getData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->master);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);

        $path = tempnam(sys_get_temp_dir(), 'aptgive_packages_');
        file_put_contents($path, gzdecode($data));

        return file($path);
    }

    private function defaultDonationRules($node)
    {
        $donation_url = false;

        if ($node['donate']) {
            /*
                If donations informations are already defined, just skip
            */
            $this->managed_urls[$node['homepage']] = $node['donate'];
        }
        else if (isset($this->managed_urls[$node['homepage']])) {
            /*
                If the homepage matches another package with donations
                informations, the same informations are inherited
            */
            $donation_url = $this->managed_urls[$node['homepage']];
        }
        else {
            /*
                Here are collected the donation URLs for most recurring websites
                assigned as "Homepage" (assuming those refer to the same main
                project)
            */
            $directs = [
                'http://gcc.gnu.org/'                                                   => 'https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57',
                'https://www.llvm.org/'                                                 => 'https://foundation.llvm.org/docs/sponsors/',
                'http://www.libreoffice.org'                                            => 'https://www.libreoffice.org/donate/',
                'https://www.tryton.org/'                                               => 'https://www.tryton.org/donate',
                'http://www.mate-desktop.org/'                                          => 'https://mate-desktop.org/donate/',
                'http://gambas.sourceforge.net'                                         => 'https://gambas.sourceforge.net/en/main.html',
                'https://wiki.documentfoundation.org/Language_support_of_LibreOffice'   => 'https://www.libreoffice.org/donate/',
                'https://www.horde.org/'                                                => 'https://www.horde.org/',
                'https://www.gnu.org/software/binutils/'                                => 'https://my.fsf.org/donate/',
                'https://xcb.freedesktop.org'                                           => 'https://www.spi-inc.org/projects/x.org/',
                'http://www.glx-dock.org/'                                              => 'http://www.glx-dock.org/',
                'http://www.tug.org/texlive/'                                           => 'https://tug.org/',
                'https://gstreamer.freedesktop.org'                                     => 'https://www.spi-inc.org/projects/x.org/',
                'https://www.enlightenment.org'                                         => 'https://www.enlightenment.org/contrib/start',
                'https://www.gnu.org/software/gcc/'                                     => 'https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57',
                'https://wiki.gnome.org/Apps/Evolution'                                 => 'https://www.gnome.org/donate/',
                'https://opencv.org'                                                    => 'https://www.paypal.com/paypalme/OpenCV',
                'http://plugins.geany.org'                                              => 'https://geany.org/about/donate/',
                'https://pcp.io'                                                        => 'https://opencollective.com/pcp',
                'https://www.eclipse.org/eclipse/platform-ui/'                          => 'https://www.eclipse.org/sponsor/ide/',
                'https://hslua.org/'                                                    => 'https://github.com/sponsors/tarleb',
                'https://www.x.org/'                                                    => 'https://www.spi-inc.org/projects/x.org/',
                'http://cinnamon.linuxmint.com/'                                        => 'https://developer.linuxmint.com/donors.php',
                'https://www.claws-mail.org'                                            => 'https://www.claws-mail.org/donations.php',
                'http://www.php.net/'                                                   => 'https://opencollective.com/phpfoundation',
                'https://kodi.tv/'                                                      => 'https://kodi.tv/donate/',
                'http://games.kde.org/'                                                 => 'https://kde.org/community/donations/',
                'http://fenicsproject.org'                                              => 'https://numfocus.org/donate-to-fenics',
                'http://www.eclipse.org/equinox/'                                       => 'https://www.eclipse.org/sponsor/ide/',
                'https://www.imagemagick.org/'                                          => 'https://github.com/sponsors/ImageMagick',
                'https://qgis.org/'                                                     => 'https://donate.qgis.org/',
                'https://manpages-l10n-team.pages.debian.net/manpages-l10n/'            => 'https://www.debian.org/donations',
                'https://github.com/fcitx/fcitx'                                        => 'https://fcitx-im.org/wiki/Donate',
                'https://www.gtk.org/'                                                  => 'https://www.gnome.org/donate/',
                'http://hdfgroup.org/HDF5/'                                             => 'https://www.hdfgroup.org/about-us/donate-to-the-hdf-group/',
                'https://www.python.org/'                                               => 'https://www.python.org/psf/donations/',
            ];

            $found = false;

            foreach($directs as $rule => $donations) {
                if ($rule == $node['homepage']) {
                    $donation_url = $donations;
                    $found = true;
                    break;
                }
            }

            if ($found == false) {
                /*
                    If the package is hosted by a well known entity, by default
                    the donations are assigned to it
                */
                $rules = [
                    // GNU packages are assigned to FSF
                    '/^https?:\/\/[^\/]*gnu.org\/?.*$/' => 'https://my.fsf.org/donate/',

                    // Packages hosted by Apache are assigned to the Apache Foundation
                    '/^https?:\/\/[^\/]*apache.org\/?.*$/' => 'https://donate.apache.org/',

                    // Packages hosted by Gnome are assigned to Gnome Foundation
                    '/^https?:\/\/[^\/]*gnome.org\/?.*$/' => 'https://www.gnome.org/donate/',

                    // Packages hosted by KDE are assigned to KDE e.V.
                    '/^https?:\/\/[^\/]*kde.org\/?.*$/' => 'https://kde.org/community/donations/',

                    // Packages hosted by XFCE are assigned to XFCE
                    '/^https?:\/\/[^\/]*xfce.org\/?.*$/' => 'https://wiki.xfce.org/finance/open-collective',

                    // Packages hosted by Freedesktop are assigned to X.org
                    '/^https?:\/\/www.freedesktop.org\/?.*$/' => 'https://www.spi-inc.org/projects/x.org/',

                    // Packages hosted by Debian are assigned to Debian
                    '/^https?:\/\/wiki.debian.org\/?.*$/' => 'https://www.debian.org/donations',
                    '/^https?:\/\/blends.debian.org\/?.*$/' => 'https://www.debian.org/donations',

                    // Packages hosted by MetaCPAN are assigned to MetaCPAN
                    '/^https?:\/\/search.cpan.org\/?.*$/' => 'https://opencollective.com/metacpan-core',
                    '/^https?:\/\/metacpan.org\/?.*$/' => 'https://opencollective.com/metacpan-core',

                    // Packages hosted by LibreOffice are assigned to The Document Foundation
                    '/^https?:\/\/[^\/]*libreoffice.org\/?.*$/' => 'https://www.libreoffice.org/donate/',

                    // Packages hosted by PECL are assigned to The PHP Foundation
                    '/^https?:\/\/pecl.php.net\/?.*$/' => 'https://opencollective.com/phpfoundation',

                    // Packages hosted by R Archive are assigned to The R Foundation
                    '/^https?:\/\/cran.r-project.org\/package=.*$/' => 'https://www.r-project.org/foundation/donations.html',
                ];

                foreach($rules as $rule => $donations) {
                    if (preg_match($rule, $node['homepage'])) {
                        $donation_url = $donations;
                        break;
                    }
                }
            }
        }

        if ($donation_url) {
            $node['donate'] = $donation_url;
            $node['updated'] = $this->today;
            $node['to_be_reviewed'] = false;
        }

        return $node;
    }

    function dumpPackage($node)
    {
        $data_file = sprintf('../data/%s', $node['name']);

        if (file_exists($data_file)) {
            $original = json_decode(file_get_contents($data_file), true);

            if ($original['homepage'] != $node['homepage']) {
                $original['homepage'] = $node['homepage'];
                $original['to_be_reviewed'] = true;
            }
        }
        else {
            $original = array_merge($node, [
                'donate' => false,
                'added' => $this->today,
                'updated' => $this->today,
                'to_be_reviewed' => true,
            ]);
        }

        $original = $this->defaultDonationRules($original);

        file_put_contents($data_file, json_encode($original, JSON_PRETTY_PRINT));
    }
}

$builder = new Builder();
$builder->run();
