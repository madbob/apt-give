<?php

class Stats
{
    private function renderChart($yes, $not, $und)
    {
        /*
            https://stackoverflow.com/a/66473249/3135371
        */

        $tot = $yes + $not + $und;

        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
            <path stroke-dasharray="' . $yes . ' ' . ($tot - $yes) . '" stroke-dashoffset="' . $yes . '" stroke="green" pathLength="' . $tot . '" stroke-width="50" d="M75 50a1 1 90 10-50 0a1 1 90 10 50 0" fill="none"></path>
            <path stroke-dasharray="' . $not . ' ' . ($tot - $not) . '" stroke-dashoffset="' . ($yes + $not) . '" stroke="red" pathLength="' . $tot . '" stroke-width="50" d="M75 50a1 1 90 10-50 0a1 1 90 10 50 0" fill="none"></path>
            <path stroke-dasharray="' . $und . ' ' . ($tot - $und) . '" stroke-dashoffset="' . $tot . '" stroke="blue" pathLength="' . $tot . '" stroke-width="50" d="M75 50a1 1 90 10-50 0a1 1 90 10 50 0" fill="none"></path>
        </svg>';
    }

    public function run()
    {
        $donations = $no_donations = $undefined = 0;

        $path = '../data/';
        $files = scandir($path);
        $final = [];

        foreach($files as $file) {
            if ($file[0] == '.') {
                continue;
            }

            $fullpath = $path . $file;

            $data = json_decode(file_get_contents($fullpath));

            if ($data->donate) {
                $donations++;
            }
            else {
                if ($data->to_be_reviewed) {
                    $undefined++;
                }
                else {
                    $no_donations++;
                }
            }
        }

        echo "With donations: " . $donations . "\n";
        echo "Without donations: " . $no_donations . "\n";
        echo "To be reviewed: " . $undefined . "\n";

        $chart = $this->renderChart($donations, $no_donations, $undefined);
        file_put_contents('../public/assets/chart.svg', $chart);
    }
}

$stats = new Stats();
$stats->run();
