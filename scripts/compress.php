<?php

class Compressor
{
    public function run()
    {
        $path = '../data/';
        $files = scandir($path);
        $final = [];

        foreach($files as $file) {
            if ($file[0] == '.') {
                continue;
            }

            $fullpath = $path . $file;

            $data = json_decode(file_get_contents($fullpath));
            if (is_string($data->donate)) {
                $final[] = (object) [
                    'name' => $file,
                    'url' => $data->donate,
                ];
            }
        }

        file_put_contents('../public/index.json', json_encode($final));
    }
}

$compressor = new Compressor();
$compressor->run();
