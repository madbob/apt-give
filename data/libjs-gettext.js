{
    "name": "libjs-gettext.js",
    "homepage": "https:\/\/github.com\/guillaumepotier\/gettext.js",
    "donate": "https:\/\/github.com\/sponsors\/guillaumepotier",
    "added": "2024-05-22",
    "updated": "2024-05-23",
    "to_be_reviewed": false
}