{
    "name": "libjs-jquery-selectize.js",
    "homepage": "https:\/\/github.com\/selectize\/selectize.js",
    "donate": "https:\/\/github.com\/sponsors\/risadams",
    "added": "2024-05-22",
    "updated": "2024-06-02",
    "to_be_reviewed": false
}