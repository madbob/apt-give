{
    "name": "libjs-select2.js",
    "homepage": "https:\/\/select2.github.io\/",
    "donate": "https:\/\/opencollective.com\/select2",
    "added": "2024-05-22",
    "updated": "2024-06-01",
    "to_be_reviewed": false
}