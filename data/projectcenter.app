{
    "name": "projectcenter.app",
    "homepage": "http:\/\/gnustep.org\/experience\/ProjectCenter.html",
    "donate": "https:\/\/my.fsf.org\/civicrm\/contribute\/transact?reset=1&id=21",
    "added": "2024-05-22",
    "updated": "2024-06-01",
    "to_be_reviewed": false
}